package app.ecommerce.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.adapter.ImageSliderAdapter;
import app.ecommerce.ui.data.DataGenerator;
import app.ecommerce.ui.model.ExampleProduct;
import app.ecommerce.ui.utils.Tools;

public class ActivityDayPlans extends AppCompatActivity implements View.OnClickListener {


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.rl_dropdown_plan_1:

                ic_dropdown_plan_1.setImageResource(ll_dropdown_plan_1.getVisibility() == View.GONE ? R.drawable.ic_keyboard_arrow_up : R.drawable.ic_keyboard_arrow_down);

                ll_dropdown_plan_1.setVisibility(ll_dropdown_plan_1.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);

                break;
            case R.id.rl_dropdown_plan_2:

                ic_dropdown_plan_2.setImageResource(ll_dropdown_plan_2.getVisibility() == View.GONE ? R.drawable.ic_keyboard_arrow_up : R.drawable.ic_keyboard_arrow_down);

                ll_dropdown_plan_2.setVisibility(ll_dropdown_plan_2.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);


                break;
            case R.id.rl_dropdown_plan_3:

                ic_dropdown_plan_3.setImageResource(ll_dropdown_plan_3.getVisibility() == View.GONE ? R.drawable.ic_keyboard_arrow_up : R.drawable.ic_keyboard_arrow_down);

                ll_dropdown_plan_3.setVisibility(ll_dropdown_plan_3.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);

                break;
            default:
                break;
        }

    }

    RecyclerView recycler_view;
    ImageSliderAdapter adapter;
    RelativeLayout rl_dropdown_plan_1, rl_dropdown_plan_2, rl_dropdown_plan_3;
    LinearLayout ll_dropdown_plan_1, ll_dropdown_plan_2, ll_dropdown_plan_3;
    ImageView ic_dropdown_plan_1,ic_dropdown_plan_2,ic_dropdown_plan_3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_plans);

        initToolbar();
        initComponent();
        initListener();

    }

    private void initListener() {
        rl_dropdown_plan_1 = findViewById(R.id.rl_dropdown_plan_1);
        rl_dropdown_plan_2 = findViewById(R.id.rl_dropdown_plan_2);
        rl_dropdown_plan_3 = findViewById(R.id.rl_dropdown_plan_3);

        ll_dropdown_plan_1 = findViewById(R.id.ll_dropdown_plan_1);
        ll_dropdown_plan_2 = findViewById(R.id.ll_dropdown_plan_2);
        ll_dropdown_plan_3 = findViewById(R.id.ll_dropdown_plan_3);

        ic_dropdown_plan_1 = findViewById(R.id.ic_dropdown_plan_1);
        ic_dropdown_plan_2 = findViewById(R.id.ic_dropdown_plan_2);
        ic_dropdown_plan_3 = findViewById(R.id.ic_dropdown_plan_3);

        rl_dropdown_plan_1.setOnClickListener(this);
        rl_dropdown_plan_2.setOnClickListener(this);
        rl_dropdown_plan_3.setOnClickListener(this);
    }

    private void initComponent() {
        initRecyclerViewPlan1();
        initRecyclerViewPlan2();
        initRecyclerViewPlan3();
    }

    private void initRecyclerViewPlan1() {
        // define recycler view
        recycler_view = findViewById(R.id.rv_images_plan_1);
        recycler_view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycler_view.setHasFixedSize(true);
        recycler_view.setNestedScrollingEnabled(false);

        List<ExampleProduct> items = DataGenerator.getProducts(this);
        items.addAll(DataGenerator.getProducts(this));
        items.addAll(DataGenerator.getProducts(this));
        items.addAll(DataGenerator.getProducts(this));

        //set data and list adapter
        adapter = new ImageSliderAdapter(this, items);
        recycler_view.setAdapter(adapter);

        // on item list clicked
        adapter.setOnItemClickListener(new ImageSliderAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, ExampleProduct obj, int position) {
                Toast.makeText(getApplicationContext(), "Item " + obj.title + " clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void initRecyclerViewPlan2() {
        // define recycler view
        recycler_view = findViewById(R.id.rv_images_plan_2);
        recycler_view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycler_view.setHasFixedSize(true);
        recycler_view.setNestedScrollingEnabled(false);

        List<ExampleProduct> items = DataGenerator.getProducts(this);
        items.addAll(DataGenerator.getProducts(this));
        items.addAll(DataGenerator.getProducts(this));
        items.addAll(DataGenerator.getProducts(this));

        //set data and list adapter
        adapter = new ImageSliderAdapter(this, items);
        recycler_view.setAdapter(adapter);

        // on item list clicked
        adapter.setOnItemClickListener(new ImageSliderAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, ExampleProduct obj, int position) {
                Toast.makeText(getApplicationContext(), "Item " + obj.title + " clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void initRecyclerViewPlan3() {
        // define recycler view
        recycler_view = findViewById(R.id.rv_images_plan_3);
        recycler_view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycler_view.setHasFixedSize(true);
        recycler_view.setNestedScrollingEnabled(false);

        List<ExampleProduct> items = DataGenerator.getProducts(this);
        items.addAll(DataGenerator.getProducts(this));
        items.addAll(DataGenerator.getProducts(this));
        items.addAll(DataGenerator.getProducts(this));

        //set data and list adapter
        adapter = new ImageSliderAdapter(this, items);
        recycler_view.setAdapter(adapter);

        // on item list clicked
        adapter.setOnItemClickListener(new ImageSliderAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, ExampleProduct obj, int position) {
                Toast.makeText(getApplicationContext(), "Item " + obj.title + " clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        //change menu button color
        Tools.changeNavigationIconColor(toolbar, getResources().getColor(R.color.grey_60));
        //change overflow menu button color
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.grey_60));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Day Plans");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.example_menu, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.grey_60));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

}
